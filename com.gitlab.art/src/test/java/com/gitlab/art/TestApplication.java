package com.gitlab.art;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestApplication 
{
	WebDriver driver;
	@BeforeTest
	public void beforeTest() 
	{	
        System.setProperty("webdriver.chrome.driver", "E:\\Jenkins_Demo\\chromedriver_win32\\chromedriver.exe");
		
		driver = new ChromeDriver();
		System.out.println("Opening browser");
	 }	
  
	@Test
 	public void testEasy() 
    {	
		driver.get("http://demo.guru99.com/test/guru99home/");  
		String Actualtitle = driver.getTitle();
		String Expectedtitle="Demo Guru99 Page";
		System.out.println("Before Assetion: Expected Result: " + Expectedtitle +", Actual result: "+ Actualtitle);
		Assert.assertEquals(Actualtitle, Expectedtitle);
		System.out.println("After Assertion: Expected Result: " + Expectedtitle +", Actual result: "+ Actualtitle + " Title matched ");
	}	
		
	@AfterTest
	public void afterTest() 
	{
		driver.quit();			
	}	
}
